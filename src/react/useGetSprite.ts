import { useEffect, useState } from 'react';
import { addEventListener, getSprite } from '../index';
import { Sprite } from '../types/Sprite';

export const useGetSprite = (src: string): Sprite | null => {
	const [sprite, setSprite] = useState<Sprite | null>(getSprite(src));

	useEffect(() => {
		const newSprite = getSprite(src);
		if (newSprite?.src == null) {
			setSprite(null);
			return;
		}
		setSprite(newSprite);
	}, [src]);

	useEffect(() => {
		if (sprite != null) {
			return;
		}

		return addEventListener('load', (event) => {
			if (event.sprite.name === src) {
				setSprite(event.sprite);
			}
		});
	}, [sprite]);

	if (sprite == null) {
		return null;
	}

	return sprite;
};
