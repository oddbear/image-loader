import React from 'react';
import { useGetSprite } from './useGetSprite';

interface Props {
	src: string;
	alt?: string;
	title?: string;
	onClick?: (e: React.MouseEvent<HTMLImageElement>) => void;
	onMouseDown?: (e: React.MouseEvent<HTMLImageElement>) => void;
	onMouseUp?: (e: React.MouseEvent<HTMLImageElement>) => void;
	onMouseEnter?: (e: React.MouseEvent<HTMLImageElement>) => void;
	onMouseLeave?: (e: React.MouseEvent<HTMLImageElement>) => void;
	onKeyDown?: (e: React.KeyboardEvent<HTMLImageElement>) => void;
	onKeyUp?: (e: React.KeyboardEvent<HTMLImageElement>) => void;
	className?: string;
}

export const SpriteView: React.FC<Props> = React.memo(function SpriteView({
	src,
	alt,
	title,
	onClick,
	onMouseDown,
	onMouseUp,
	onMouseEnter,
	onMouseLeave,
	onKeyDown,
	onKeyUp,
	className,
}) {
	const sprite = useGetSprite(src);

	if (sprite == null) {
		return null;
	}

	return (
		<img
			alt={alt ?? `sprite with src ${src}`}
			src={sprite.src}
			title={title}
			onClick={onClick}
			onMouseDown={onMouseDown}
			onMouseUp={onMouseUp}
			onMouseEnter={onMouseEnter}
			onMouseLeave={onMouseLeave}
			onKeyDown={onKeyDown}
			onKeyUp={onKeyUp}
			className={className}
		/>
	);
});
