import '../../__mocks__/image.mock';
import '../../__mocks__/canvas.mock';
import '../../__mocks__/url.mock';
import React from 'react';
import TestRenderer from 'react-test-renderer';
import { SpriteView } from '../SpriteView';
import { getSpriteImage, loadSprites } from '../../index';
import { SEPARATOR } from '../../utils/constants';

const dataUri = `data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==${SEPARATOR}0,0,1,1`;

const findByTag = (root: TestRenderer.ReactTestInstance, tag: string): TestRenderer.ReactTestInstance[] => {
	return root.findAll((el) => el.type === tag);
};

describe('react component SpriteViewer', () => {
	it('define src after spriteSheet loaded', async () => {
		let testRenderer: ReturnType<typeof TestRenderer.create>;
		await TestRenderer.act(async () => {
			testRenderer = TestRenderer.create(<SpriteView src={dataUri} />);
		});

		const root = testRenderer!.root;
		expect(findByTag(root, 'img')).toHaveLength(0);

		await TestRenderer.act(async () => {
			await loadSprites([dataUri]);
		});

		expect(getSpriteImage(dataUri)).toBeTruthy();
		expect(findByTag(root, 'img')).not.toHaveLength(0);
	});
});
