import { SEPARATOR } from './utils/constants';

export class SpriteSheetSampler {
	public readonly samples: string[] = [];
	private isGreedy: boolean = false;

	constructor(private readonly baseUrl: string) {}

	public join(
		x: number,
		y: number,
		width: number,
		height: number,
		rotate?: number,
	): Omit<SpriteSheetSampler, 'makeGreedy'> {
		this.samples.push(this.makeSample(x, y, width, height, rotate));

		return this;
	}

	// returns url with full sized image
	public makeGreedy(): string {
		if (this.samples.length > 0) {
			throw new Error(`can't make greedy sampler if it already contains a samples`);
		}

		this.isGreedy = true;
		this.samples.push(this.baseUrl);

		return this.baseUrl;
	}

	public makeSample(x: number, y: number, width: number, height: number, rotate?: number): string {
		if (this.isGreedy) {
			throw new Error(`can't join samples to greedy sampler`);
		}

		return `${this.baseUrl}${SEPARATOR}${x.toString()},${y.toString()},${width.toString()},${height.toString()}${
			rotate !== undefined ? `,${rotate.toString()}` : ''
		}`;
	}
}
