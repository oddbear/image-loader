import { parseSrcWithOptions } from '../parser';
import { DATA_URL } from '../constants';

describe('function parseSrcWithOptions', () => {
	it('accept string with 0 parameters', () => {
		expect(parseSrcWithOptions(DATA_URL)).toStrictEqual([DATA_URL, 0, 0, null, null, 0]);
	});
});
