import { SEPARATOR } from './constants';
import { SrcOptions } from '../SpriteSheetController';

export const parseSrcWithOptions = (srcWithOptions: string): SrcOptions => {
	const urlSplit = srcWithOptions.split(SEPARATOR);
	if (urlSplit.length > 2) {
		throw new Error(
			`incorrect srcWithOptions parameter, should looks like: https://domain.com/image.png${SEPARATOR}{x},{y},{width},{height},{rotate?}, but got: ${srcWithOptions}`,
		);
	}
	const urlOptions = (urlSplit[1] as string | undefined)?.split(',') ?? ['0', '0', null, null];
	if (urlOptions.length < 4 || urlOptions.length > 5) {
		throw new Error(
			`incorrect format of options, should be 5 numbers in a row: "x", "y", "width", "height" and optional parameter "rotate", but got: ${srcWithOptions}`,
		);
	}

	const left = parseInt(urlOptions[0] as string);
	const top = parseInt(urlOptions[1] as string);
	if (Number.isNaN(left) || Number.isNaN(top)) {
		throw new Error(`incorrect format of left or top parameters should be numbers, got: ${srcWithOptions}`);
	}

	return [
		urlSplit[0],
		left,
		top,
		urlOptions[2] == null ? null : parseInt(urlOptions[2]),
		urlOptions[3] == null ? null : parseInt(urlOptions[3]),
		urlOptions[4] == null ? 0 : parseInt(urlOptions[4]),
	];
};
