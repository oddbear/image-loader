export const getNewProps = (
	width: number,
	height: number,
	radians: number,
): {
	width: number;
	height: number;
} => {
	const c0 = getPoint(width / 2, height / 2, 0, 0, radians);
	const c1 = getPoint(width / 2, height / 2, width, 0, radians);
	const c2 = getPoint(width / 2, height / 2, width, height, radians);
	const c3 = getPoint(width / 2, height / 2, 0, height, radians);

	const { xs, ys } = [c0, c1, c2, c3].reduce<{
		xs: number[];
		ys: number[];
	}>(
		(acc, { x, y }) => ({
			xs: acc.xs.concat(x),
			ys: acc.ys.concat(y),
		}),
		{
			xs: [],
			ys: [],
		},
	);

	const minX = Math.min(...xs);
	const minY = Math.min(...ys);
	const maxX = Math.max(...xs);
	const maxY = Math.max(...ys);

	return {
		width: maxX - minX,
		height: maxY - minY,
	};
};

export const getPoint = (
	mx: number,
	my: number,
	cx: number,
	cy: number,
	radians: number,
): {
	x: number;
	y: number;
} => {
	// get distance from center to point
	const diffX = cx - mx;
	const diffY = cy - my;
	const dist = Math.sqrt(diffX * diffX + diffY * diffY);

	// find angle from pivot to corner
	const ca = Math.atan2(diffY, diffX);

	// get new angle based on old + current delta angle
	const na = ca + radians;

	// get new x and y and round it off to integer
	const x = mx + dist * Math.cos(na) + 0.5;
	const y = my + dist * Math.sin(na) + 0.5;

	return { x, y };
};
