import { SpriteSheetSampler } from '../SpriteSheetSampler';
import { SEPARATOR } from '../utils/constants';

const joinBaseAndCoords = (base: string, coords: number[][]): string[] =>
	coords.map(
		([x, y, w, h, r]) =>
			`${base}${SEPARATOR}${x.toString()},${y.toString()},${w.toString()},${h.toString()}${
				r != null ? ',' + r.toString() : ''
			}`,
	);

describe('class SpriteSheetSampler', () => {
	it('method join joins base url and coordinates to single url and can be chained', () => {
		const base = 'MeoW Sir! What the shell??!';
		const sampler = new SpriteSheetSampler(base);

		const coords = [
			[0, 11, -14, 256664544],
			[9 / 11, /* to */ Infinity /* and beyond! */, 3.14, 13],
		];

		expect(
			// @ts-ignore
			sampler.join(...coords[0]).join(...coords[1]).samples,
		).toEqual(joinBaseAndCoords(base, coords));
	});
});
