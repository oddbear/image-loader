import 'fake-indexeddb/auto';
import '../__mocks__/image.mock';
import '../__mocks__/canvas.mock';
import '../__mocks__/url.mock';
import localforage from 'localforage';
import { SpriteSheetSampler } from '../SpriteSheetSampler';
import { addMiddleware, clearMiddlewares, getSprite, getSpriteSheet, loadSprites } from '../index';
import { Middleware } from '../SpriteSheetController';
import { parseSrcWithOptions } from '../utils/parser';
import { DATA_URL } from '../utils/constants';

describe('class SpriteSheetController', () => {
	beforeEach(() => {
		clearMiddlewares();
	});

	it('method chainMiddleware stores right values', async () => {
		const middlewareCallback: Middleware = (src, left, top, ...args) => [src + '.ending', left + 1, top - 1, ...args];
		addMiddleware(middlewareCallback);

		const testSheetSampler = new SpriteSheetSampler(DATA_URL);
		const [icon] = testSheetSampler.join(1, 2, 3, 4).samples;

		await loadSprites([icon]);

		expect(getSprite(icon)).toBeTruthy();
		expect(getSpriteSheet(DATA_URL)).toBeTruthy();

		const parsedOptions = middlewareCallback(...parseSrcWithOptions(icon));
		const sampler = new SpriteSheetSampler(parsedOptions[0]);
		const actualSrc = sampler.join(
			parsedOptions[1],
			parsedOptions[2],
			parsedOptions[3]!,
			parsedOptions[4]!,
			parsedOptions[5],
		).samples[0];
		expect(localforage.getItem(actualSrc)).toBeTruthy();
	});

	describe('method loadSprites', () => {
		it('makes sprite sheet contain uploaded sprites with src with options', async () => {
			const testSheetSampler = new SpriteSheetSampler(DATA_URL);
			const [icon1, icon2] = testSheetSampler.join(1, 2, 3, 4).join(1, 2, 3, 5).samples;

			await loadSprites([icon1, icon2]);

			expect(getSprite(icon1)).toBeTruthy();
			expect(getSprite(icon2)).toBeTruthy();
		});

		it('one time loading process for sprites with same urls', async () => {
			await loadSprites([DATA_URL]);
			const oldSprite = getSprite(DATA_URL);
			await loadSprites([DATA_URL]);

			expect(oldSprite).toBe(getSprite(DATA_URL));
		});
	});
});
