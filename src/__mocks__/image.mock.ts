Object.defineProperty(global, 'Image', {
	get() {
		return class Image {
			public width: number = 256;
			public height: number = 300;
			public onload: undefined | (() => void);

			constructor() {
				setTimeout(() => {
					this.onload?.();
				}, 10);
			}

			public addEventListener(eventName: string, callback: () => void): void {
				setTimeout(callback, 11);
			}
		};
	},
});

export {};
