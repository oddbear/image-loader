const dataUrl = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

Object.defineProperty(global, 'URL', {
	get() {
		return {
			createObjectURL: () => dataUrl,
		};
	},
});

export {};
