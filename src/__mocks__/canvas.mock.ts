const dataUrl = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

const nativeFunction = document.createElement.bind(document);
Object.defineProperty(document, 'createElement', {
	get() {
		return (elementName: 'canvas', ...args) => {
			switch (elementName) {
				case 'canvas':
					return {
						toDataURL: () => dataUrl,
						toBlob: (callback: (blob: string) => void) => {
							callback(dataUrl);
						},
						getContext: () => ({
							save: () => {
								return;
							},
							restore: () => {
								return;
							},
							drawImage: () => {
								return;
							},
						}),
					};

				default:
					return nativeFunction(elementName, ...args);
			}
		};
	},
});

export {};
