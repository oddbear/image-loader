import localforage from 'localforage';
import { getNewProps } from './utils/transformation';
import { GreedySprite, Sprite } from './types/Sprite';

interface AddSpriteOptions {
	onload?: (src: string) => void;
}

export class SpriteSheet {
	public readonly spritesMap: Map<string, Sprite> = new Map();
	private readonly baseImage: HTMLImageElement;
	private isBaseImageLoaded: boolean = false;

	constructor(public readonly src: string, private readonly options?: AddSpriteOptions) {
		const baseImage = (this.baseImage = new Image());
		baseImage.crossOrigin = 'anonymous';
		baseImage.addEventListener('load', () => {
			this.isBaseImageLoaded = true;
			options?.onload?.(src);
		});
		baseImage.src = src;
	}

	public getSpriteByName(name: string): Sprite {
		const sprite = this.spritesMap.get(name);
		if (sprite == null) {
			// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
			throw new Error(`sprite with name "${name}" not found in SpriteSheet with src "${this.src}"`);
		}
		return sprite;
	}

	public addSprite(spriteOptions: GreedySprite): Promise<Sprite> {
		spriteOptions.src = null;

		return new Promise((res) => {
			this.loadSprite(spriteOptions, (src) => {
				spriteOptions.src = src;

				this.spritesMap.set(spriteOptions.name, spriteOptions as Sprite);

				res(spriteOptions as Sprite);
			});
		});
	}

	private loadSprite(spriteOptions: GreedySprite, onload?: (src: string) => void): void {
		const callback = (): void => {
			if (spriteOptions.width == null || spriteOptions.height == null) {
				spriteOptions.width = this.baseImage.width;
				spriteOptions.height = this.baseImage.height;
			}

			this.spriteToUrl(spriteOptions as Sprite, onload);
		};
		this.isBaseImageLoaded ? callback() : this.baseImage.addEventListener('load', callback);
	}

	private spriteToUrl(sprite: Sprite, onload?: (src: string) => void): void {
		const canvas = document.createElement('canvas');
		const oldWidth = (canvas.width = sprite.width);
		const oldHeight = (canvas.height = sprite.height);

		const ctx = canvas.getContext('2d');
		if (ctx == null) {
			throw new Error(`sprite not rendered because of context2D not defined ${this.baseImage.src}`);
		}
		ctx.save();

		let offsetX = 0;
		let offsetY = 0;
		if (sprite.rotate !== 0) {
			const radians = (sprite.rotate / 180) * Math.PI;

			const { width, height } = getNewProps(sprite.width, sprite.height, radians);

			canvas.width = width;
			canvas.height = height;

			offsetX = -sprite.width / 2 - (canvas.width - oldWidth) / 2;
			offsetY = -sprite.height / 2 + (canvas.height - oldHeight) / 2;

			ctx.translate(sprite.left + sprite.width / 2, sprite.top + sprite.height / 2);
			ctx.rotate(radians);
		}

		ctx.drawImage(
			this.baseImage,
			sprite.left,
			sprite.top,
			sprite.width,
			sprite.height,
			offsetX,
			offsetY,
			sprite.width,
			sprite.height,
		);

		ctx.restore();

		const load = (blob: Blob): void => {
			const url = URL.createObjectURL(blob);
			onload?.(url);
		};

		const itemNotFoundCallback = (): void => {
			canvas.toBlob((value) => {
				if (value == null) {
					throw new Error('blob can not be created');
				}
				load(value);
				void localforage.setItem(sprite.name, value);
			});
		};

		try {
			void localforage.getItem(sprite.name, (err, value) => {
				if (err != null || value == null) {
					itemNotFoundCallback();
					return;
				}
				load(value as Blob);
			});
		} catch (err) {
			itemNotFoundCallback();
		}
	}
}
