export interface Sprite {
	src: string;
	originSrc: string;
	name: string;
	left: number;
	top: number;
	width: number;
	height: number;
	rotate: number;
}

export type GreedySprite = Omit<Sprite, 'width' | 'height' | 'src'> & {
	width: number | null;
	height: number | null;
	src: string | null;
};
