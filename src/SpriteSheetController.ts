import { SpriteSheet } from './SpriteSheet';
import { GreedySprite, Sprite } from './types/Sprite';
import { parseSrcWithOptions } from './utils/parser';

export interface Event {
	sprite: Sprite;
}

export type SrcOptions = [string, number, number, number | null, number | null, number];

export type Middleware = (
	src: string,
	left: number,
	top: number,
	width: number | null,
	height: number | null,
	rotate: number,
) => SrcOptions;

export class SpriteSheetController {
	private readonly spriteSheetsMap: Map<string, SpriteSheet> = new Map();
	private readonly spritesMap: Map<string, GreedySprite> = new Map();
	private readonly events: { [key: string]: ((event: Event) => void)[] } = {
		load: [],
	};
	private middlewares: Middleware[] = [];

	constructor() {
		this.getSprite = this.getSprite.bind(this);
		this.getSpriteSheet = this.getSpriteSheet.bind(this);
		this.getSpriteImage = this.getSpriteImage.bind(this);
		this.getSpriteBySheetAndName = this.getSpriteBySheetAndName.bind(this);
		this.loadSprites = this.loadSprites.bind(this);
		this.addEventListener = this.addEventListener.bind(this);
		this.addMiddleware = this.addMiddleware.bind(this);
		this.clearMiddlewares = this.clearMiddlewares.bind(this);
		this.chainMiddlewares = this.chainMiddlewares.bind(this);
	}

	public addMiddleware(callback: Middleware): void {
		this.middlewares.push(callback);
	}

	public clearMiddlewares(): void {
		this.middlewares = [];
	}

	public chainMiddlewares(
		src: string,
		left: number,
		top: number,
		width: number | null,
		height: number | null,
		rotate: number,
	): SrcOptions {
		return this.middlewares.reduce((args, callback) => callback(...args), [src, left, top, width, height, rotate]);
	}

	public getSpriteSheet(src: string): SpriteSheet | undefined {
		return this.spriteSheetsMap.get(src);
	}

	public getSpriteBySheetAndName(spriteSheetSrc: string, spriteName: string): Sprite | undefined {
		return this.getSpriteSheet(spriteSheetSrc)?.getSpriteByName(spriteName);
	}

	public getSprite(spriteUrl: string): Sprite | null {
		const sprite = this.spritesMap.get(spriteUrl);
		if (sprite == null || sprite.src == null) {
			return null;
		}
		return sprite as Sprite;
	}

	public getSpriteImage(spriteUrl: string): HTMLImageElement {
		const sprite = this.spritesMap.get(spriteUrl);

		const image = new Image();
		if (sprite?.src != null) {
			image.src = sprite.src;
		} else {
			const removeListener = this.addEventListener('load', (event) => {
				if (event.sprite.name === spriteUrl) {
					image.src = event.sprite.src;
					removeListener();
				}
			});
		}

		return image;
	}

	public loadSprites(sourcesWithOptions: string[]): Promise<Sprite[]> {
		const promises: Promise<Sprite>[] = [];
		// eslint-disable-next-line @typescript-eslint/prefer-for-of
		for (let i = 0; i < sourcesWithOptions.length; i++) {
			const srcWithOptions = sourcesWithOptions[i];

			const [originalSrc, ...options] = parseSrcWithOptions(srcWithOptions);
			const [src, left, top, width, height, rotate] = this.chainMiddlewares(originalSrc, ...options);

			const oldSprite = this.spritesMap.get(srcWithOptions);
			if (oldSprite != null) {
				continue;
			}

			const spriteOptions = {
				src: null,
				originSrc: src,
				name: srcWithOptions,
				left,
				top,
				width,
				height,
				rotate,
			};
			this.spritesMap.set(srcWithOptions, spriteOptions);

			let spriteSheet = this.getSpriteSheet(src);
			if (spriteSheet === undefined) {
				spriteSheet = new SpriteSheet(src);
				this.spriteSheetsMap.set(originalSrc, spriteSheet);
			}

			promises.push(
				spriteSheet.addSprite(spriteOptions).then((sprite) => {
					this.fireEvents('load', {
						sprite,
					});
					return sprite;
				}),
			);
		}

		return Promise.all(promises);
	}

	public addEventListener(
		eventName: keyof typeof SpriteSheetController.prototype.events,
		callback: (event: Event) => void,
	): () => void {
		this.events[eventName].push(callback);
		// remove event listener callback
		return () => {
			this.events[eventName].splice(this.events[eventName].indexOf(callback), 1);
		};
	}

	private fireEvents(eventName: keyof typeof SpriteSheetController.prototype.events, event: Event): void {
		this.events[eventName].forEach((callback) => callback(event));
	}
}
