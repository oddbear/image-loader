import { SpriteSheetController } from './SpriteSheetController';
export { SpriteSheetController } from './SpriteSheetController';

const spriteSheetController = new SpriteSheetController();
export const getSprite = spriteSheetController.getSprite;
export const getSpriteSheet = spriteSheetController.getSpriteSheet;
export const getSpriteImage = spriteSheetController.getSpriteImage;
export const getSpriteBySheetAndName = spriteSheetController.getSpriteBySheetAndName;
export const loadSprites = spriteSheetController.loadSprites;
export const addEventListener = spriteSheetController.addEventListener;
export const addMiddleware = spriteSheetController.addMiddleware;
export const clearMiddlewares = spriteSheetController.clearMiddlewares;
export const chainMiddlewares = spriteSheetController.chainMiddlewares;
